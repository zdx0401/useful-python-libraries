# Computational algorithms

## Token based

### [fuzzywuzzy](https://github.com/seatgeek/fuzzywuzzy)

Multiple variations of the Levenshtein distance. Wrapper over difflib.
