# Software Development Tools

## Command Line Related

### [python-fire](https://github.com/google/python-fire)

Wrapper over argparse

### [argparse](https://docs.python.org/3/library/argparse.html)

Create simple command line interfaces for any python object.

## Testing

### [pytest](https://docs.pytest.org/en/latest/)

Test library with support of other test libraries like unittest. Interesting function and class structure. 

## Development

### [pgeocode](https://pgeocode.readthedocs.io/en/latest/index.html)

Get longitude and latitude coordinates as well as other geographic information.

### [functools](https://docs.python.org/3/library/functools.html)

Functional programming tools such as partial evaluation, lru cache, and other cool stuff.

### [multiprocessing](https://docs.python.org/3.7/library/multiprocessing.html)

Multiprocessing tools such as `.map` allowing us to compute function application on chunks in different processes. Also can be used for asynchronous functions.
